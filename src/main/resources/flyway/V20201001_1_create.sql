create table if not exists person
(
    id bigint
        constraint person_pkey
            primary key,
    firstname varchar(255),
    lastname varchar(255),
    city varchar(255),
    phone varchar(255),
    telegram varchar(255)
);
alter table person owner to postgres;

