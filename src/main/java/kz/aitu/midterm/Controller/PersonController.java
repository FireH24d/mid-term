package kz.aitu.midterm.Controller;

import kz.aitu.midterm.Entity.Person;
import kz.aitu.midterm.Service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class PersonController {
    @Autowired
    private  PersonService personService;
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(value="/api/v2/users/",method=RequestMethod.GET, headers = "Accept=application/json")
    public List<Person> getAll(Model model) {
        List<Person> personList = personService.getAll();
        model.addAttribute("item", new Person());
        model.addAttribute("personList",personList);
        return personList;
    }
    @RequestMapping(value="/api/v2/users/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(personService.getById(id));}
    @RequestMapping(value = "/api/v2/users/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        personService.delete(id);
    }
    @RequestMapping(value="/api/v2/users",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody Person person){
        return ResponseEntity.ok(personService.update(person));
    }
    /*
    getlist: GET http://127.0.0.1:8081/api/v2/users/
add and edit: POST http://127.0.0.1:8081/api/v2/users/
delete by id: DELETE  http://127.0.0.1:8081/api/v2/users/{id}
*/
}
