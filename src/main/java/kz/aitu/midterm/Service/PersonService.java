package kz.aitu.midterm.Service;

import kz.aitu.midterm.Repository.PersonRepository;
import kz.aitu.midterm.Entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }
    public Optional<Person> getById(long id) {
        return personRepository.findById(id);
    }

    public void delete(long id){
        personRepository.deleteById(id);
    }
    public Person update(@RequestBody Person person){
        return  personRepository.save(person);
    }

}
